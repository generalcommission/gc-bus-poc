from python_liftbridge import Lift, Stream, ErrStreamExists

from src.transformer.field.transformer import GCFieldTransformer


def main():
    # Create a Liftbridge client.
    client = Lift(ip_address='localhost:9292')
    subject = 'gc'
    stream_name = 'fields'

    try:
        client.create_stream(Stream(subject=subject, name=stream_name))
        print('This stream has been created!')
    except ErrStreamExists:
        print('This stream already exists!')

    transformer = GCFieldTransformer(
        server='127.0.0.1:9292',
        subject=subject,
        stream_name=stream_name,
        field_id_1='field_1',
        field_id_2='field_2'
    )
    transformer.start_listening()


main()
