import time

from src.entity.gc_field import GCField
from src.provider.field.emitter import GCFieldEmitter


def main():
    stream_name = 'fields'

    field_1 = GCField(
        key='field_1',
        value='field_1_value'
    )
    field_2 = GCField(
        key='field_2',
        value='field_2_value'
    )

    emitter = GCFieldEmitter(
        server='127.0.0.1:9292',
        stream_name=stream_name
    )

    emitter.emit_field_to_stream(field_1)
    time.sleep(5)
    emitter.emit_field_to_stream(field_2)


main()
