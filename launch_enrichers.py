from python_liftbridge import Lift, Stream, ErrStreamExists

from src.enricher.field.enricher import GCFieldEnricher


def main():
    # Create a Liftbridge client.
    client = Lift(ip_address='localhost:9292')
    subject = 'gc'
    stream_name = 'fields'

    try:
        client.create_stream(Stream(subject=subject, name=stream_name))
        print('This stream has been created!')
    except ErrStreamExists:
        print('This stream already exists!')

    enricher = GCFieldEnricher(
        server='127.0.0.1:9292',
        subject=subject,
        stream_name=stream_name,
        transformed_field_id_1='transformed_field_1_field_2',
        transformed_field_id_2='transformed_field_id_2'
    )
    enricher.start_listening()

main()
