import jsonpickle
import datetime
import time

from python_liftbridge import Message
from python_liftbridge import Lift
from python_liftbridge import Stream

from src.entity.gc_transformed_entity import GCTransformedEntity
from src.entity.gc_enriched_entity import GCEnrichedEntity


class GCFieldEnricher:

    def __init__(
            self,
            server: str,
            subject: str,
            stream_name: str,
            transformed_field_id_1: str,
            transformed_field_id_2: str
    ):
        self.client = Lift(ip_address=server)

        self.subject = subject
        self.stream_name = stream_name

        self.transformed_field_id_1 = transformed_field_id_1
        self.transformed_field_id_2 = transformed_field_id_2
        self.transformed_field_value_1 = None
        self.transformed_field_value_2 = None

    def start_listening(self):

        print('Started listening in GCFieldEnricher!')
        for message in self.client.subscribe(
                Stream(self.subject, self.stream_name).start_at_earliest_received()):

            if not message.value:
                continue
            transformed_field = jsonpickle.decode(message.value)
            if transformed_field.__class__.__name__ != 'GCTransformedEntity':
                continue

            update_transformed_field_1 = (transformed_field.key == self.transformed_field_id_1) and \
                                         not self.transformed_field_value_1

            #update_transformed_field_2 = (transformed_field.key == self.transformed_field_id_2) and \
            #                             not self.transformed_field_value_2

            self.transformed_field_value_1 = transformed_field.key if update_transformed_field_1 else self.transformed_field_value_1
            #self.transformed_field_value_2 = transformed_field.key if update_transformed_field_2 else self.transformed_field_value_2

            time.sleep(5)
            print("{} - GCFieldEnricher received [{}]: '{}'".format(datetime.datetime.now(), self.stream_name, transformed_field))
            if self.transformed_field_value_1:

                gc_transformed_field_1 = GCTransformedEntity(self.transformed_field_id_1,
                                                             self.transformed_field_value_1)

                #gc_transformed_field_2 = GCTransformedEntity(self.transformed_field_id_2,
                #                                             self.transformed_field_value_2)

                enriched_entity = self.enrich_v1(gc_transformed_field_1=gc_transformed_field_1)
                                              #gc_transformed_field_2=gc_transformed_field_2)

                print("GCFieldEnricher published [{}]: '{}'".format(self.stream_name, enriched_entity))
                self.client.publish(Message(value=jsonpickle.encode(enriched_entity), stream=self.stream_name))

                self.transformed_field_value_1 = None
                #self.transformed_field_value_2 = None

    def enrich_v1(self, gc_transformed_field_1: GCTransformedEntity):
        enriched_id = 'enriched_' + self.transformed_field_id_1 + '_' + self.transformed_field_id_2
        enriched_entity = GCEnrichedEntity(key=enriched_id, value=[gc_transformed_field_1])
        return enriched_entity

    def enrich_v2(self, gc_transformed_field_1: GCTransformedEntity, gc_transformed_field_2: GCTransformedEntity):
        enriched_id = 'enriched_' + self.transformed_field_id_1 + '_' + self.transformed_field_id_2
        enriched_entity = GCEnrichedEntity(key=enriched_id, value=[gc_transformed_field_1, gc_transformed_field_2])
        return enriched_entity
