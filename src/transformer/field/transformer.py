import jsonpickle
import datetime
import time

from python_liftbridge import Message
from python_liftbridge import Lift
from python_liftbridge import Stream

from src.entity.gc_field import GCField
from src.entity.gc_transformed_entity import GCTransformedEntity


class GCFieldTransformer:

    def __init__(
            self,
            server: str,
            subject: str,
            stream_name: str,
            field_id_1: str,
            field_id_2: str
    ):
        self.client = Lift(ip_address=server)

        self.subject = subject
        self.stream_name = stream_name

        self.field_id_1 = field_id_1
        self.field_id_2 = field_id_2
        self.field_value_1 = None
        self.field_value_2 = None

    def start_listening(self):
        print('Started listening in GCFieldTransformer!')

        for message in self.client.subscribe(
                Stream(self.subject, self.stream_name)
                .start_at_earliest_received()):

            if not message.value:
                continue

            field = jsonpickle.decode(message.value)
            if field.__class__.__name__ != 'GCField':
                continue

            update_field_1 = field.key == self.field_id_1 and not self.field_value_1
            update_field_2 = field.key == self.field_id_2 and not self.field_value_2

            self.field_value_1 = field.key if update_field_1 else self.field_value_1
            self.field_value_2 = field.key if update_field_2 else self.field_value_2

            print("{} - GCFieldTransformer received [{}]: '{}'".format(datetime.datetime.now(),self.stream_name, field))
            if self.field_value_1 and self.field_value_2:

                gc_field_1 = GCField(self.field_id_1, self.field_value_1)
                gc_field_2 = GCField(self.field_id_2, self.field_value_2)

                transformed_entity = self.transform(gc_field_1=gc_field_1, gc_field_2=gc_field_2)
                time.sleep(5)
                print("{} - GCFieldTransformer published [{}]: '{}'".format(datetime.datetime.now(),self.stream_name, transformed_entity))
                self.client.publish(Message(value=jsonpickle.encode(transformed_entity), stream=self.stream_name))

                self.field_value_1 = None
                self.field_value_2 = None

    def transform(self, gc_field_1: GCField, gc_field_2: GCField):
        transformed_id = 'transformed_' + self.field_id_1 + '_' + self.field_id_2
        transformed_entity = GCTransformedEntity(key=transformed_id, value=[gc_field_1, gc_field_2])
        return transformed_entity
