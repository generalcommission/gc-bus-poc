import jsonpickle
import datetime
import time

from python_liftbridge import Message
from python_liftbridge import Lift
from python_liftbridge import Stream

from src.entity.gc_enriched_entity import GCEnrichedEntity
from src.entity.gc_aligned_entity import GCAlignedEntity


class GCFieldAligner:

    def __init__(
            self,
            server: str,
            subject: str,
            stream_name: str,
            enriched_field_id_1: str,
            enriched_field_id_2: str
    ):
        self.client = Lift(ip_address=server)

        self.subject = subject
        self.stream_name = stream_name

        self.enriched_field_id_1 = enriched_field_id_1
        self.enriched_field_id_2 = enriched_field_id_2
        self.enriched_field_value_1 = None
        self.enriched_field_value_2 = None

    def start_listening(self):

        print('Started listening in GCFieldAligner!')
        for message in self.client.subscribe(
                Stream(self.subject, self.stream_name).start_at_earliest_received()):

            if not message.value:
                continue
            enriched_field = jsonpickle.decode(message.value)
            if enriched_field.__class__.__name__ != 'GCEnrichedEntity':
                continue

            update_enriched_field_1 = (enriched_field.key == self.enriched_field_id_1) and \
                                         not self.enriched_field_value_1

            #update_enriched_field_2 = (enriched_field.key == self.enriched_field_id_2) and \
            #                             not self.enriched_field_value_2

            self.enriched_field_value_1 = enriched_field.key if update_enriched_field_1 else self.enriched_field_value_1
            #self.enriched_field_value_2 = enriched_field.key if update_enriched_field_2 else self.enriched_field_value_2

            print("{} - GCFieldAligner received [{}]: '{}'".format(datetime.datetime.now(), self.stream_name, enriched_field))
            if self.enriched_field_value_1:

                gc_enriched_field_1 = GCEnrichedEntity(self.enriched_field_id_1,
                                                       self.enriched_field_value_1)

                #gc_enriched_field_2 = GCEnrichedEntity(self.enriched_field_id_2,
                #                                       self.enriched_field_value_2)

                aligned_entity = self.align_v1(gc_enriched_field_1=gc_enriched_field_1)

                time.sleep(5)
                print("{} - GCFieldAligner published [{}]: '{}'".format(datetime.datetime.now(), self.stream_name, aligned_entity))
                self.client.publish(Message(value=jsonpickle.encode(aligned_entity), stream=self.stream_name))

                self.enriched_field_value_1 = None
                self.enriched_field_value_2 = None

    def align_v1(self, gc_enriched_field_1: GCEnrichedEntity):
        enriched_id = 'enriched_' + self.enriched_field_id_1
        enriched_entity = GCAlignedEntity(key=enriched_id, value={'gc_transformed_field_1': gc_enriched_field_1})
        return enriched_entity

    def align_v2(self, gc_enriched_field_1: GCEnrichedEntity, gc_enriched_field_2: GCEnrichedEntity):
        enriched_id = 'enriched_' + self.enriched_field_id_1 + '_' + self.enriched_field_id_2
        enriched_entity = GCAlignedEntity(key=enriched_id, value={'gc_transformed_field_1': gc_enriched_field_1,
                                                                  'gc_transformed_field_2': gc_enriched_field_2})
        return enriched_entity
