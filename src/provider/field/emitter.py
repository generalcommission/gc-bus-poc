import jsonpickle
import datetime

from python_liftbridge import Lift
from python_liftbridge import Message

from src.entity.gc_field import GCField


class GCFieldEmitter:

    def __init__(self, server: str, stream_name: str):
        self.client = Lift(ip_address=server)
        self.stream = stream_name

    def emit_field_to_stream(self, field: GCField):
        print("{} - GCFieldEmitter published [{}]: '{}'".format(datetime.datetime.now(),self.stream, field))
        self.client.publish(Message(value=jsonpickle.encode(field), stream=self.stream))
