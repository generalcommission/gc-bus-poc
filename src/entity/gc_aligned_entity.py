
from src.entity.gc_enriched_entity import GCEnrichedEntity


class GCAlignedEntity(object):
    def __init__(self, key: str, value: {GCEnrichedEntity}):
        self.key = key
        self.value = value
