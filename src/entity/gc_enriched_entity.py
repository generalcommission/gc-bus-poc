
from src.entity.gc_transformed_entity import GCTransformedEntity


class GCEnrichedEntity(object):
    def __init__(self, key: str, value: [GCTransformedEntity]):
        self.key = key
        self.value = value
