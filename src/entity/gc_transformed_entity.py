
from src.entity.gc_field import GCField


class GCTransformedEntity(object):
    def __init__(self, key: str, value: [GCField]):
        self.key = key
        self.value = value
